###
PersonController.js

@description ::
@docs        :: http://sailsjs.org/#!documentation/controllers
###
module.exports =
  index: (req, res) ->
    Person.find().exec (err, ps) ->
      res.view people: ps
      return
    return

  edit: (req, res) ->
    id = req.param('id')
    Person.findOne(id).exec (err, person) ->
      res.send err, 500 if err
      res.send "No such person", 404 if !person

      res.view
        person: person
        days:
          "1": "Segunda"
          "2": "Terça"
          "3": "Quarta"
          "4": "Quinta"
          "5": "Sexta"
      return
    return

  save: (req, res) ->
    id = req.param('id')
    person = req.query
    person.id = id
    Person.update {id: id}, person, (err, updated) ->
      if err
        return res.view 'person/edit', {
          person: person
          errors: err
        }, 401
      return res.send "No such person", 404 if !updated

      res.redirect '/person'
